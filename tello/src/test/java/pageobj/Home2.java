package pageobj;

import org.apache.hc.core5.reactor.Command.Priority;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.KeyDownAction;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.microsoft.edge.seleniumtools.EdgeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class Home2 {
	WebDriver driver =null;
	//ExtentHtmlReporter html;
	//ExtentReports er,er2;

//	@BeforeSuite
//	public void extent() {
//		//html = new ExtentHtmlReporter("reporter.html");
//		//er = new ExtentReports();
//		//er.attachReporter(html);
//		
//	}

	@BeforeTest
	public void setup() {
		System.out.println("This is setup");
		WebDriverManager.chromedriver().setup();
		driver= new ChromeDriver();
	}
	
	@Test
	public void FB() {
		ExtentTest tes=Home.er.createTest("FB search test1","This is a test to valudate fb search");
		tes.info("Test to search in FB starts");
		driver.get("https://www.fb.com");
		tes.pass("Navigated to google");
		System.out.println("completed FB search");
	}
	
	@Test
	public void youtube() {
		ExtentTest tes=Home.er.createTest("youtube search test1","This is a test to valudate YT search");
		tes.info("Test to search in YT starts");
		driver.get("https://www.youtube.com");
		tes.pass("Navigated to YT");
		
		System.out.println("completed YT search");
		
	}

	@AfterTest
	public void teardown() {
		
		System.out.println("This is teardown");
		driver.close();
	}
	
//	@AfterSuite
//	public void extentend() {
//		er.flush();
//	}
}