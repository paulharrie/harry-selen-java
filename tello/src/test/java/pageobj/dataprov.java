package pageobj;

import org.apache.hc.core5.reactor.Command.Priority;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.KeyDownAction;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.microsoft.edge.seleniumtools.EdgeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class dataprov {
	WebDriver driver =null;
	
	
	
	
	@BeforeTest
	public void setup() {
		System.out.println("This is setup");
		WebDriverManager.chromedriver().setup();
		driver= new ChromeDriver();
	}
	
	@Test(dataProvider = "data")
	public void google(String s, String v) {
		System.out.println("Test to search in google starts");
		driver.get("https://www.google.com");
		System.out.println("Navigated to google");
		driver.findElement(By.name("q")).sendKeys(s);
		System.out.println("Entered search text");
		driver.findElement(By.name("q")).sendKeys(Keys.RETURN);
		System.out.println("Pressed enter key");
		System.out.println("completed google search");
	}
	
	@DataProvider()
	public Object[][] data() {
		return new Object[][] {{"billy","vary"},{"billy8","varyk"}};
		
	}

	@AfterTest
	public void teardown() {
		
		System.out.println("This is teardown");
		driver.close();
	}
	
}