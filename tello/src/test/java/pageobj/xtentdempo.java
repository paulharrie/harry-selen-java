package pageobj;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

import io.github.bonigarcia.wdm.WebDriverManager;

public class xtentdempo {

	public static void main(String[] args) {
		ExtentHtmlReporter html = new ExtentHtmlReporter("er.html");
		ExtentReports er = new ExtentReports();
		er.attachReporter(html);
		ExtentTest tes=er.createTest("Google search test1","This is a test to valudate google search");
		WebDriverManager.chromedriver().setup();
		WebDriver driver= new ChromeDriver();
		tes.info("Test to search in google starts");
		driver.get("https://google.com");
		tes.pass("Navigated to google");
		driver.findElement(By.name("q")).sendKeys("harry");
		tes.pass("Entered search text");
		driver.findElement(By.name("q")).sendKeys(Keys.RETURN);
		tes.pass("Pressed enter key");
		System.out.println("completed search");
		er.flush();
	}
	
}
