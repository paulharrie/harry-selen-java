package pageobj;

import java.util.NoSuchElementException;
import java.util.concurrent.TimeUnit;

import org.apache.hc.core5.reactor.Command.Priority;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.interactions.KeyDownAction;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Ignore;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;
import com.google.common.base.Function;
import com.microsoft.edge.seleniumtools.EdgeDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

@Listeners(listners.Itest.class)
public class Home {
	WebDriver driver =null;
	public ExtentHtmlReporter html;
	public static ExtentReports er;
	
	@BeforeSuite
	public void extent() {
		System.out.println("B4 suit starts");
		html = new ExtentHtmlReporter("reporter.html");
		er = new ExtentReports();
		er.attachReporter(html);
		
	}
	
	@BeforeTest
	public void setup() {
		System.out.println("This is setup");
		WebDriverManager.chromedriver().setup();
		driver= new ChromeDriver();
	}
	
	@Test
	public void google() {
		ExtentTest tes=er.createTest("Google search test1","This is a test to valudate google search");
		tes.info("Test to search in google starts");
		driver.get("https://www.google.com");
		tes.pass("Navigated to google");
		driver.findElement(By.name("q")).sendKeys("harry");
		tes.pass("Entered search text");
		driver.findElement(By.name("q")).sendKeys(Keys.RETURN);
		tes.pass("Pressed enter key");
		System.out.println("completed google search");
		//driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
		WebDriverWait waitt = new WebDriverWait(driver, 10);
		waitt.until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//h3[contains(text(),'Top stories')]")));
		//driver.findElement(By.xpath("//h3[contains(text(),'Top stories')]"));
		

	}
	@Test()
	public void amazon() {
		ExtentTest tes=er.createTest("Amazon search test1","This is a test to valudate amazon search");
		tes.info("Test to search in google starts");
		driver.get("https://www.amazon.com");
		tes.pass("Navigated to amazon");
//		driver.findElement(By.name("field-keywords")).sendKeys("harry");
//		tes.pass("Entered search text");
//		driver.findElement(By.name("field-keywords")).sendKeys(Keys.RETURN);
//		tes.pass("Pressed enter key");
		System.out.println("completed amazon search");
		
	}

	@AfterTest
	public void teardown() {
		
		System.out.println("This is teardown");
		driver.close();
	}
	@AfterSuite
	public void extentend() {
		er.flush();
		System.out.println("Aftr suit ends");
	}
}