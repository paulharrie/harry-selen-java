package listners;

import org.testng.IRetryAnalyzer;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

public class Itest implements ITestListener, IRetryAnalyzer{
	private int retrycount=0;
	public static final int MAX=3;
	public boolean retry(ITestResult result) {
		if(retrycount<MAX) {
			retrycount++;
			return true;
		}
		return false;
	}

	public void onTestStart(ITestResult result) {
		System.out.println("@@@@ Test started "+result.getName());
	}

	public void onTestSuccess(ITestResult result) {
		System.out.println("@@@@ Test success "+result.getName());

	}

	public void onTestFailure(ITestResult result) {
		System.out.println("@@@@ Test failed "+result.getName());

		
	}

	public void onTestSkipped(ITestResult result) {
		System.out.println("@@@@ Test skipped "+result.getName());

	}

	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
	

	}

	public void onTestFailedWithTimeout(ITestResult result) {
		
	}

	public void onStart(ITestContext context) {
		
	}

	public void onFinish(ITestContext context) {
		
	}
	
	

}
